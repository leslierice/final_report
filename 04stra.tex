Strassen's algorithm is a matrix-matrix multiplication algorithm that reduces the number of multiplications in the operation \fromto{C := $\alpha$AB + $\beta$C}{$ C := \alpha AB + \beta C $} from 8 to 7 \cite{Strassen1969}. Strassen's algorithm is widely considered to only be practical when multiplying very large and square matrices. However, with the BLIS approach for matrix-matrix multiplication, Strassen's algorithm has been proven to be practical for small and rectangular matrices \cite{StrassenReloaded}. To demonstrate Strassen's algorithm, let’s assume \fromto{\textit{m}, \textit{n}, and \textit{k}}{$ m $, $ n $, and $ k $} are even, and the matrices \fromto{A, B, and C}{$ A $, $ B $, and $ C $} have been partitioned as follows. \fromto{\\}{}

\[
C
=
\begin{bmatrix}
    C_{00} & C_{01} \\
    C_{10} & C_{11} \\
\end{bmatrix},
A
=
\begin{bmatrix}
    A_{00} & A_{01} \\
    A_{10} & A_{11} \\
\end{bmatrix},
B
=
\begin{bmatrix}
    B_{00} & B_{01} \\
    B_{10} & B_{11} \\
\end{bmatrix}
\]
\fromto{\\}{}

A direct computation of \fromto{C := AB + C}{$ C := AB + C $} using partitions is the following set of operations. Note that there are 8 multiplications and 8 additions.

\fromto{
\[
\textit{C}_{00} := A_{00}B_{00} + A_{01}B_{10} + C_{00}; \\
C_{01} := A_{00}B_{01} + A_{01}B_{11} + C_{01}; \\
C_{10} := A_{10}B_{00} + A_{11}B_{10} + C_{10}; \\
C_{11} := A_{10}B_{01} + A_{11}B_{11} + C_{11}; \\
\] }{
\[
\begin{array}{l c l}
C_{00} &:=& A_{00} B_{00} + A_{01} B_{10} + C_{00} \\
C_{01} &:=& A_{00} B_{01} + A_{01} B_{11} + C_{01} \\
C_{10} &:=& A_{10} B_{00} + A_{11} B_{10} + C_{10} \\
C_{11} &:=& A_{10} B_{01} + A_{11} B_{11} + C_{11} 
\end{array}
\]
}

Using one level of Strassen's algorithm, we have the following set of operations, with 7 multiplications and 22 additions.\fromto{ \\ \\ }{}

\fromto{
\[
\textit{M}_0 := (A_{00} + A_{11})(B_{00} + B_{11}) \\
M_1 := (A_{10} + A_{11})B_{00}; \\
M_2 := A_{00}(B_{01} – B_{11}); \\
M_3 := A_{11}(B_{10} – B_{00}); \\
M_4 := (A_{00} + A_{01})B_{11}; \\
M_5 := (A_{10} – A_{00})(B_{00} + B_{01}); \\
M_6 := (A_{01} – A_{11})(B_{10} + B_{11}); \\
\textit{C}_{00} := M_0 + M_3 – M_4 + M_7 + C_{00}; \\
C_{01} := M_2 + M_4 + C_{01}; \\
C_{10} := M_1 + M_3 + C_{10}; \\
C_{11} := M_0 – M_1 + M_2 + M_5 + C_{11} \\
\] }{

{
\begin{equation*}
\begin{array}{ l c l }  
M_0 &:=& ( A_{00} + A_{11} ) ( B_{00} + B_{11} );  \\
M_1 &:=&  ( A_{10} + A_{11} ) B_{00};  \\
M_2 &:=&  A_{00} ( B_{01} - B_{11} ); \\
M_3 &:=&  A_{11}( B_{10} - B_{00} );  \\
M_4 &:=&  ( A_{00} + A_{01}) B_{11};   \\
M_5&:=&  (A_{10} - A_{00} )( B_{00} + B_{01} );  \\
M_6&:=&  (A_{01} - A_{11} )( B_{10} + B_{11} );  \\
C_{00} &:=& M_0 + M_3 – M_4 + M_7 + C_{00}; \\
C_{01} &:=& M_2 + M_4 + C_{01}; \\
C_{10} &:=& M_1 + M_3 + C_{10}; \\
C_{11} &:=& M_0 – M_1 + M_2 + M_5 + C_{11} \\
\end{array}
\label{eqn:stra}
\end{equation*}
}

}


By comparing direct computation and Strassen's algorithm, we see that the computational cost is \fromto{2\textit{mnk}}{$ 2 m n k $} flops versus \fromto{(7/8)2\textit{mnk}}{$ (7/8)2 m n k $} flops. Strassen's algorithm can be performed as follows to give a general operation for one-level Strassen as outlined in  \cite{StrassenReloaded}.\fromto{\\ \\}{}

\fromto{
\[
\textit{M}_0 := (A_{00} + A_{11})(B_{00} + B_{11}); \\
C_{00} += M_0; C_{11} += M_0; \\
M_1 := (A_{10} + A_{11})B_{00}; \\
C_{10} += M_1; C_{11} –= M_1; \\
M_2 := A_{00} (B_{01} – B_{11}); \\
C_{01} += M_2; C_{11} += M_2; \\
M_3 := A_{11}(B_{10} – B_{00}); \\
C_{00} += M_3;C_{10} += M_3; \\
M_4 := (A_{00} + A_{01})B_{11}; \\
C_{01} += M_4; C_{00} –= M_4; \\
M_5 := (A_{10} – A_{00})(B_{00} + B_{01}); \\
C_{11} += M_5; \\
M_6 := (A_{01} – A_{11})(B_{10} + B_{11}); \\
C_{00} += M_6; \\
\] }{
{
\begin{equation*}
\begin{array}{l @{\hspace{1pt}} c @{\hspace{1pt}} l l r}  
M_0 &=& ( A_{00} + A_{11} ) ( B_{00} + B_{11} ); 
% \\ & 
& 
C_{00} +\!\!= M_0;  C_{11} +\!\!= M_0;  \\   
M_1 &=&  ( A_{10} + A_{11} ) B_{00}; 
% \\ & 
& 
C_{10} +\!\!= M_1 ;  C_{11} -\!\!= M_1 ; \\  
M_2 &=&  A_{00} ( B_{01} - B_{11} ); 
% \\ & 
&  
C_{01} +\!\!= M_2 ;  C_{11} +\!\!= M_2 ;
\\  
M_3 &=&  A_{11}( B_{10} - B_{00} ); 
% \\ & 
&  
C_{00} +\!\!= M_3 ;  C_{10} +\!\!= M_3 ;
\\  
M_4 &=&  ( A_{00} + A_{01}) B_{11};  
% \\ & 
& 
C_{01} +\!\!=  M_4 ;   C_{00} -\!\!= M_4;
\\  
M_5&=&  (A_{10} - A_{00} )( B_{00} + B_{01} ); 
% \\ & 
& 
C_{11} +\!\!= M_5;  
\\  
M_6&=&  (A_{01} - A_{11} )( B_{10} + B_{11} );  
% \\ & 
& 
C_{00} +\!\!= M_6 ; 
\end{array}
\label{eqn:allops}
\end{equation*}
}
}

Thus the general operation for one-level Strassen is the following:

\begin{equation}
M := (X + \delta Y)(V + \epsilon W);
C += \gamma_0 M; D += \gamma_1 M;
\label{e:genops}
\end{equation} \fromto{\\}{}


